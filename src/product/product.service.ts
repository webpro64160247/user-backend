import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
let product: Product[] = [
  { id: 1, name: 'Administrator', price: 1000 },
  { id: 2, name: 'user1', price: 1000 },
  { id: 3, name: 'user2', price: 1000 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log(product[index]);
    // console.log(updateProductDto);
    const updateProduct: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = product[index];
    product.splice(index, 1);
    return deleteProduct;
  }
  reset() {
    product = [
      { id: 1, name: 'Administrator', price: 1000 },
      { id: 2, name: 'user1', price: 1000 },
      { id: 3, name: 'user2', price: 1000 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
